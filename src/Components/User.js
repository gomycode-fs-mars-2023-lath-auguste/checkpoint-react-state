import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';

function User() {
    const [validated, setValidated] = useState(false);
    const [nom, setNom]= useState("");
    const [prenom, setPrenom]= useState("");
    const [email, setEmail] = useState("");
    const [gender, setGender] = useState('')

    const [personnes, setPersonnes] = useState([])

    const handleSubmit = (event) => {
        event.preventDefault()
    const form = event.currentTarget;
        if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        }
        
        setPersonnes([...personnes, {nom, prenom, email, gender}]);
        setValidated(true);

        

    };


    return (
        <>
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
        <Row className="mb-3">
            <Form.Group as={Col} md="10" className='mx-3' controlId="validationCustom01">
            <Form.Label>Nom</Form.Label>
            <Form.Control
                required
                type="text"
                placeholder="Nom"
                defaultValue={nom}
                onChange={(e)=>setNom(e.target.value)}

            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="10" className='mx-3' controlId="validationCustom02">
            <Form.Label>Prenom</Form.Label>
            <Form.Control
                required
                type="text"
                placeholder="Prenom"
                defaultValue={prenom}
                onChange={(e)=>setPrenom(e.target.value)}

            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="10" className='mx-3' controlId="validationCustomUsername">
            <Form.Label>Email</Form.Label>
            <InputGroup hasValidation>
                {/* <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text> */}
                <Form.Control
                type="email"
                placeholder="Username"
                aria-describedby="inputGroupPrepend"
                required
                defaultValue={email}
                onChange={(e)=>setEmail(e.target.value)}

                />
                <Form.Control.Feedback type="invalid">
                Please choose a email.
                </Form.Control.Feedback>
            </InputGroup>
            </Form.Group>
        </Row>
        
        <Row className="mb-3">
            <Form.Group as={Col} md="10" className='mx-3'>
                <Form.Label>Sexe</Form.Label>
                <div>
                <Form.Check
                    required
                    type="radio"
                    label="Male"
                    name="gender"
                    id="male"
                    value='male'
                    checked={gender === 'male'}
                    onChange={(e) => setGender(e.target.value)}
                />
                <Form.Check
                    required
                    type="radio"
                    label="Female"
                    name="gender"
                    id="female"
                    value='female'
                    checked={gender === 'female'}
                    onChange={(e) => setGender(e.target.value)}
                />
                </div>
            </Form.Group>
        </Row>

        <Form.Group className="mb-3 mx-3">
            <Form.Check
            required
            label="Agree to terms and conditions"
            feedback="You must agree before submitting."
            feedbackType="invalid"
            />
        </Form.Group>
        <Button type="submit" className='mx-3'>Submit form</Button>
        </Form>

        
        {/* La liste */}

        <Row className='mx-2 py-5'>
            <h2>La liste</h2>
            <Table striped bordered hover>
            <thead>
                <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Email</th>
                <th>Sexe</th>
                </tr>
            </thead>
            <tbody>
            {personnes.map((pers, index) => <tr key={index}>
                <td>{index + 1} </td>
                <td>{pers.nom} </td>
                <td>{pers.prenom }</td>
                <td>{pers.email }</td>
                <td>{pers.gender}</td>
            </tr>)}
                
            </tbody>

            </Table>
            
        </Row>

        <div className='py-5'></div>

        </>
    );
}

export default User;